<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function about()
    {
        return view('about');
    }
    public function contact()
    {
        return view('contact');
    }
    public function index()
    {
        return view('index');
    }
    public function single()
    {
        return view('single-product');
    }
    public function home()
    {
        return view('home');
    }
}
