<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/about', 'IndexController@about');

Route::get('/contact', 'IndexController@contact');

Route::get('index', 'IndexController@index');

Route::get('/single-product', 'IndexController@single');

Route::get('/', 'HomeController@index');

Route::get('/master', function () {
    return view('layouts.master');
});

Auth::routes();

Route::get('/home', 'IndexController@home')->name('home');
